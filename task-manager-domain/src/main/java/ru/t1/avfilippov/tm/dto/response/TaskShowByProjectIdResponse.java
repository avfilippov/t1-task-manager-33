package ru.t1.avfilippov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByProjectIdResponse extends AbstractTaskResponse {

    private List<Task> tasks;

    public TaskShowByProjectIdResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}
