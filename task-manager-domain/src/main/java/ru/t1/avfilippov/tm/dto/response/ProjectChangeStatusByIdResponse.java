package ru.t1.avfilippov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@Nullable final Project project) {
        super(project);
    }

}
