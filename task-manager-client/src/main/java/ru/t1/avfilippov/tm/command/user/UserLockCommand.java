package ru.t1.avfilippov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.dto.request.UserLockRequest;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-lock";

    @NotNull
    private final String DESCRIPTION = "lock user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOCK]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setLogin(login);
        getUserEndpoint().lockUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
