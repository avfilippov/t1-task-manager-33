package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.ProjectCompleteByIndexRequest;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "complete project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @NotNull
    @Override
    public void execute() throws Exception {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest();
        request.setIndex(index);
        getProjectEndpoint().completeProjectByIndex(request);
    }

}
