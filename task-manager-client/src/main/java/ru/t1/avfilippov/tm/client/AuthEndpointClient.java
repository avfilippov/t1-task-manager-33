package ru.t1.avfilippov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.client.IAuthEndpointClient;
import ru.t1.avfilippov.tm.dto.request.UserLoginRequest;
import ru.t1.avfilippov.tm.dto.request.UserLogoutRequest;
import ru.t1.avfilippov.tm.dto.request.UserProfileRequest;
import ru.t1.avfilippov.tm.dto.response.UserLoginResponse;
import ru.t1.avfilippov.tm.dto.response.UserLogoutResponse;
import ru.t1.avfilippov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @NotNull
    public  UserLoginResponse login(
            @NotNull final UserLoginRequest request
    ) throws Exception {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @NotNull
    public UserLogoutResponse logout(
            @NotNull final UserLogoutRequest request
    ) throws Exception {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @NotNull
    public UserProfileResponse profile(
            @NotNull final UserProfileRequest request
    ) throws Exception {
        return call(request, UserProfileResponse.class);
    }


}

