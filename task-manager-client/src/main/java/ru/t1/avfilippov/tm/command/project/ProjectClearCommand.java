package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "remove all projects";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull ProjectClearRequest request = new ProjectClearRequest();
        getProjectEndpoint().clearProject(request);
    }

}
