package ru.t1.avfilippov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.client.IDomainEndpointClient;
import ru.t1.avfilippov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected IDomainEndpointClient getDomainEndpoint() {
        return serviceLocator.getDomainEndpointClient();
    }


    public AbstractDataCommand() {
    }

}
