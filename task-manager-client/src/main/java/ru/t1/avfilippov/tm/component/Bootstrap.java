package ru.t1.avfilippov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.avfilippov.tm.api.client.IEndpointClient;
import ru.t1.avfilippov.tm.api.repository.ICommandRepository;
import ru.t1.avfilippov.tm.api.service.ICommandService;
import ru.t1.avfilippov.tm.api.service.ILoggerService;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.api.service.IServiceLocator;
import ru.t1.avfilippov.tm.client.*;
import ru.t1.avfilippov.tm.command.AbstractCommand;
import ru.t1.avfilippov.tm.command.server.ConnectCommand;
import ru.t1.avfilippov.tm.command.server.DisconnectCommand;
import ru.t1.avfilippov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.avfilippov.tm.exception.system.CommandNotSupportedException;
import ru.t1.avfilippov.tm.repository.CommandRepository;
import ru.t1.avfilippov.tm.service.CommandService;
import ru.t1.avfilippov.tm.service.LoggerService;
import ru.t1.avfilippov.tm.service.PropertyService;
import ru.t1.avfilippov.tm.util.SystemUtil;
import ru.t1.avfilippov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.avfilippov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    @Getter
    @NotNull
    private final SystemEndpointClient systemEndpointClient = new SystemEndpointClient();

    @Getter
    @NotNull
    private final DomainEndpointClient domainEndpointClient = new DomainEndpointClient();

    @Getter
    @NotNull
    private final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final TaskEndpointClient taskEndpointClient = new TaskEndpointClient();

    @Getter
    @NotNull
    private final UserEndpointClient userEndpointClient = new UserEndpointClient();

    @Getter
    @NotNull
    private final AuthEndpointClient authEndpointClient = new AuthEndpointClient();


    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }


    public void run(final String[] args) {
        if (processArguments(args)) System.exit(0);
        processCommands();
    }

    private void connect() throws Exception {
        processCommand(ConnectCommand.NAME);
    }

    private void disconnect() throws Exception {
        processCommand(DisconnectCommand.NAME);
    }


    public void processCommands() {
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup()  {
        try {
            initPID();
            Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
            connect();
            fileScanner.init();
            loggerService.info("*** WELCOME TO TASK MANAGER CLIENT ***");
        } catch (@NotNull final Exception e) {
            loggerService.info("[INIT FAIL]");

        }
    }

    private void prepareShutdown() {
        fileScanner.stop();
        try {
            disconnect();
        } catch (@NotNull final Exception e) {
            getLoggerService().error(e);
        }
        loggerService.info("*** TASK MANAGER IS SHUTTING DOWN***");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        try {
            processArgument(argument);
            return true;
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return false;
        }
    }


    public void processArgument(@Nullable final String argument) throws Exception {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) throws Exception {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) throws Exception {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
