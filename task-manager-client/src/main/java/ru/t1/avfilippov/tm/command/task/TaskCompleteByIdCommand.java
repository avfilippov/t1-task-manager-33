package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.TaskCompleteByIdRequest;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand{

    @NotNull
    @Override
    public String getDescription() {
        return "complete task by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setTaskId(id);
        getTaskEndpoint().completeTaskById(request);
    }
}
