package ru.t1.avfilippov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.client.IEndpointClient;
import ru.t1.avfilippov.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

}
