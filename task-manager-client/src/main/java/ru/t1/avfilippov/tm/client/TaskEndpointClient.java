package ru.t1.avfilippov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.client.ITaskEndpointClient;
import ru.t1.avfilippov.tm.dto.request.*;
import ru.t1.avfilippov.tm.dto.response.*;
import ru.t1.avfilippov.tm.exception.AbstractException;
import ru.t1.avfilippov.tm.exception.field.AbstractFieldException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.exception.user.AccessDeniedException;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    public TaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) throws AbstractException {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) throws AbstractException {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskClearResponse clearTask(
            @NotNull final TaskClearRequest request
    ) throws UserIdEmptyException, AccessDeniedException {
        return call(request, TaskClearResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskCreateResponse createTask(
            @NotNull final TaskCreateRequest request
    ) throws AbstractFieldException {
        return call(request, TaskCreateResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskShowByIdResponse getTaskById(
            @NotNull final TaskShowByIdRequest request
    ) throws AbstractException {
        return call(request, TaskShowByIdResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskShowByIndexResponse getTaskByIndex(
            @NotNull final TaskShowByIndexRequest request
    ) throws AbstractException {
        return call(request, TaskShowByIndexResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskListResponse listTask(
            @NotNull final TaskListRequest request
    ) throws AccessDeniedException, UserIdEmptyException {
        return call(request, TaskListResponse.class);
    }

    @Override
    @NotNull
    public TaskRemoveByIdResponse removeTaskById(
            @NotNull final TaskRemoveByIdRequest request
    ) throws Exception {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @Override
    @NotNull
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @NotNull final TaskRemoveByIndexRequest request
    ) throws Exception {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @Override
    @NotNull
    public TaskStartByIdResponse startTaskById(
            @NotNull final TaskStartByIdRequest request
    ) throws Exception {
        return call(request, TaskStartByIdResponse.class);
    }

    @Override
    @NotNull
    public TaskStartByIndexResponse startTaskByIndex(
            @NotNull final TaskStartByIndexRequest request
    ) throws Exception {
        return call(request, TaskStartByIndexResponse.class);
    }

    @Override
    @NotNull
    public TaskCompleteByIdResponse completeTaskById(
            @NotNull final TaskCompleteByIdRequest request
    ) throws Exception {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @Override
    @NotNull
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @NotNull final TaskCompleteByIndexRequest request
    ) throws Exception {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @Override
    @NotNull
    public BindTaskToProjectResponse bindTaskToProject(
            @NotNull final BindTaskToProjectRequest request
    ) throws Exception {
        return call(request, BindTaskToProjectResponse.class);
    }

    @Override
    @NotNull
    public UnbindTaskFromProjectResponse unbindTaskToProject(
            @NotNull final UnbindTaskFromProjectRequest request
    ) throws Exception {
        return call(request, UnbindTaskFromProjectResponse.class);
    }

    @Override
    @NotNull
    public TaskUpdateByIdResponse updateTaskById(
            @NotNull final TaskUpdateByIdRequest request
    ) throws Exception {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @Override
    @NotNull
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @NotNull final TaskUpdateByIndexRequest request
    ) throws Exception {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @Override
    @NotNull
    public TaskShowByProjectIdResponse listTasksToProjectId(
            @NotNull final TaskShowByProjectIdRequest request
    ) throws Exception {
        return call(request, TaskShowByProjectIdResponse.class);
    }

}
