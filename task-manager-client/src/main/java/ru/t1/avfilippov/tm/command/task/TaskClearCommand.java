package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "remove all tasks";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        getTaskEndpoint().clearTask(request);
    }

}
