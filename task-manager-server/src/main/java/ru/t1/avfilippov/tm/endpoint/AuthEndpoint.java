package ru.t1.avfilippov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.service.IServiceLocator;

import javax.jws.WebService;


@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

}
